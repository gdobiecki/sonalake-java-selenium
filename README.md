Technology used: `java 1.8.0_151`, `maven 3.6.3`.

To start a test, just simply run `mvn clean install` in a terminal.

Test report can be found here: `target/surefire-reports/index.html`.
