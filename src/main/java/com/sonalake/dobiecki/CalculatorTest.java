package com.sonalake.dobiecki;

import com.sonalake.dobiecki.pageobjects.Calculator;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class CalculatorTest extends TestBase {
    private Calculator calculator;
    private final HashMap<String, String> calculations = new HashMap<>();

    @BeforeTest
    public void acceptCookies() {
        calculator = new Calculator(driver);
        wait.until(ExpectedConditions.visibilityOf(calculator.getInputField()));
        calculator.clickInputField();
        wait.until(ExpectedConditions.elementToBeClickable(calculator.getAcceptCookiesButton()));
        calculator.acceptCookies();
    }

    @BeforeTest
    public void prepareTestData() {
        //Key: input data, Value: expected output
        calculations.put("35*999+(100/4)", "34990");
        calculations.put("cos(pi)", "-1");
        calculations.put("sqrt(81)", "9");
    }

    @Test
    public void shouldDoBasicCalculationAndShowHistory() {
        wait.until(ExpectedConditions.visibilityOf(calculator.getInputField()));
        for (Map.Entry<String, String> entry: calculations.entrySet()) {
            calculator.clearAll();
            calculator.pressRadButton();
            calculator.enterValue(entry.getKey());
            calculator.pressEqualsButton();
            wait.until(ExpectedConditions.textToBePresentInElementValue(calculator.getInputField(), entry.getValue()));
            Assert.assertEquals(calculator.getInputField().getAttribute("value"), entry.getValue());
        }
        calculator.showHistory();
        calculator.getHistoryItems().forEach((item) -> Assert.assertTrue(Arrays.asList(calculations.keySet().toArray()).contains(item.getText())));
    }
}
