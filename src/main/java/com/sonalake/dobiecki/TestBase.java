package com.sonalake.dobiecki;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;

import java.time.Duration;

public class TestBase {
    protected WebDriver driver;
    protected WebDriverWait wait;

    @BeforeTest
    @Parameters({"baseUri"})
    public void setup(String baseUri) {
        String os = System.getProperty("os.name").toLowerCase();
        if (os.contains("linux")) {
            System.setProperty("webdriver.chrome.driver", "src/main/resources/webdrivers/linux/chromedriver");
        }
        if (os.contains("win")) {
            System.setProperty("webdriver.chrome.driver", "src/main/resources/webdrivers/windows/chromedriver.exe");
        }
        if (os.contains("mac")) {
            System.setProperty("webdriver.chrome.driver", "src/main/resources/webdrivers/macos/chromedriver");
        }
        driver = new ChromeDriver();
        wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        driver.get(baseUri);
    }
    @AfterTest
    public void afterTest() {
        driver.quit();
    }
}
