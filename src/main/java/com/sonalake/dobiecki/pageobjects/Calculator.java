package com.sonalake.dobiecki.pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class Calculator {
    WebDriver driver;

    @FindBy(id = "input")
    WebElement inputField;
    @FindBy(id = "BtnCalc")
    WebElement equalsButton;
    @FindBy(css = "button[value='all']")
    WebElement acceptAllCookiesButton;
    @FindBy(id = "BtnClear")
    WebElement clearButton;
    @FindBy(id = "trigorad")
    WebElement radianButton;
    @FindBy(id = "hist")
    WebElement historyDropdown;
    @FindBys({@FindBy(css = "#histframe li > p:nth-child(2)")})
    List<WebElement> historyItems;

    public Calculator(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void enterValue(String value) {
        inputField.sendKeys(value);
    }

    public WebElement getInputField() {
        return this.inputField;
    }

    public void clickInputField() {
        this.inputField.click();
    }

    public void pressEqualsButton() {
        this.equalsButton.click();
    }

    public WebElement getAcceptCookiesButton() {
        return this.acceptAllCookiesButton;
    }

    public void acceptCookies() {
        this.acceptAllCookiesButton.click();
    }

    public void clearAll() {
        this.clearButton.click();
    }

    public void pressRadButton() {
        this.radianButton.click();
    }

    public void showHistory() {
        this.historyDropdown.click();
    }

    public List<WebElement> getHistoryItems() {
        return this.historyItems;
    }
}
